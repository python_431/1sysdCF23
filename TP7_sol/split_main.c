#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "split.h"

int main(int argc, char *argv[]) {
	char *words[50];
	int n;

	if (argc != 2) {
		printf("Usage: %s \"sentence of spaces separated words\"\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	n = split(argv[1], words);	
	printf("%d: ", n);
	for ( int i = 0; i < n; i++ ) {
		printf( i == 0 ? "":" " );
		printf("\"%s\"", words[i]);
	}
	for ( int i = 0; i < n; i++ ) {
		free(words[i]);
	}
	printf("\n");
	exit(EXIT_SUCCESS);
}
