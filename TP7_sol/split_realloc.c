#include<stdlib.h>

// décompose une phrase en tableau de mots
// utiliser malloc pour allouer l'espace mémoire
// de chaque élément du tableau
// ... = malloc( sizeof(char) * longueur + 1)
// ne pas oublier qu'une chaîne c'est un tableau de char 
// avec un zéro terminal
// veriante avec realloc et pointeur vers pointeur vers pointeur
// vers char (:-D)

int split(char *msg, char ***words) {
	char *p;
	char *start;
	int i; // indice pour parcourir chaque mot trouvé
	int n; // nombre de mots trouvés

	p = msg;
	n = 0;
	//words = NULL;
	// on avance jusqu'au premier caractère non-espace
	while ( *p == ' ' ) {
		p++;
	}
	start = p;
	while (1) {
	   if ( *p == ' ' || *p == '\0' ) { // fin du mot dépassée
		// p - start est la longueur du mot
		// 0 si on est sur des espaces jusqu'à la fin
		if ( p == start ) {
			break;
		}
		*words = reallocarray(*words, n + 1, sizeof(char *));
		(*words)[n] = malloc(sizeof(char) * (p - start + 1));
		for (i = 0; i < p - start; i++) {
			(*words)[n][i] = *(start + i);
		}	
		// ne pas oublier le 0 terminal
	        (*words)[n++][i] = '\0';  // ou 0	
		// on avance tant que (et si) des espaces supplémentaires sont présents
		// pour se placer au début du mot suivant
	   	while ( *(p + 1) == ' ' ) {
			p++;
	   	}
		start = p + 1;
	   } 
	   if ( *p == '\0') {
		   break;
	   }
   	   p++; 
	}
	return n;
}

void free_list(char **words, int n) {
	for ( int i = 0; i < n; i++) {
		free(words[i]);
	}
	free(words);
}

