#include<stdio.h>
#include<stdlib.h>

// décompose une phrase en tableau de mots
// utiliser malloc pour alloer l'espace mémoire
// de chaque élément du tableau
// ... = malloc( sizeof(char) * longueur + 1)
// ne pas oublier qu'une chaîne c'est un tableau de char 
// avec un zéro terminal

int main() {
	char msg[] = "to be or not to be";
	char *words[50];
	char *p;
	char *start;
	int n; // nombre de mots trouvés
	int i; // indice pour parcourir chaque mot trouvé

	p = start = msg;
	n = 0;
	// ou encore : for (;;)
	while (1) { // boucle infinie (break plus loin)
	   if ( *p == ' ' || *p == '\0' ) { // fin du mot dépassée
		// p - start est la longueur du mot
		words[n] = malloc(sizeof(char) * (p - start + 1));
		// on copie caractère par caractère le mot dans words[n]
		for (i = 0; i < p - start; i++) {
			words[n][i] = *(start + i);
		}
		// ne pas oublier le 0 terminal
	        words[n++][i] = '\0';  // ou 0	
		start = p + 1;
	   } 
	   // si fin de chaîne : on a finit, le dernier mot a été 
	   // traité
	   if ( *p == '\0') {
		   break;
	   }
	   p++;
	}
	// une fois que c'est fait :
	for ( int i = 0; i < n; i++) {
		printf("\"%s\"\n", words[i]);
	}
	return 0;
}
