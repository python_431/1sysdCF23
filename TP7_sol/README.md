# Découpage des mots d'une phrase

Spécification : à partir d'une phrase passée en argument unique,
écrire un programme qui construit le tableau de chaînes de caractères
contenant les mots constituant cette phrase. On considérera que les
mots sont séparés par une suite d'espaces et on prendra soin d'ignorer
les espaces en début et en fin de phrase :

~~~~Bash
$ ./split "   to    be or not  to be  "
6: "to" "be" "or" "not" "to" "be"
$
~~~~

## Version 1 : préalocation d'un tableau de 50 (nombre de mots maximum) chaînes.

## Version 2 : réalocation du tableau au fur et à mesure

## Version 3 : reprise de la version 2 en utilisant memcpy ou memmove au lieu d'une boucle qui reparcourt chaque mot
