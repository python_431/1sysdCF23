#!/usr/bin/env bash

split=$1

source ./test_func.sh

echo "************** Testing program: ${split} ************************"

fail=0
passed=0

correct='6: "to" "be" "or" "not" "to" "be"'

for txt in "to be or not to be" "to   be     or not to be" "   to be or   not to be" "to be or   not to be   " "  to   be or not  to be   "
do
    if test_split ${split} "${txt}" "${correct}"
    then
	passed=$(( $passed + 1 ))
        echo -e "${green}PASSED!${norm}"
    else
	fail=$(( $fail + 1 ))
	echo -e "${red}FAILED...{$norm}"
    fi
    echo
done

txt="hamlet"
correct='1: "hamlet"'

if test_split ${split} "${txt}" "${correct}"
then
    passed=$(( $passed + 1 ))
    echo -e "${green}PASSED!${norm}"
else
    fail=$(( $fail + 1 ))
    echo -e "${red}FAILED...${norm}"
fi
echo

txt=""
correct="0: "

if test_split ${split} "${txt}" "${correct}"
then
    passed=$(( $passed + 1 ))
    echo -e "${green}PASSED!${norm}"
else
    fail=$(( $fail + 1 ))
    echo -e "${red}FAILED...${norm}"
fi
echo

txt="a"
correct='1: "a"'

if test_split ${split} "${txt}" "${correct}"
then
    passed=$(( $passed + 1 ))
    echo -e "${green}PASSED!${norm}"
else
    fail=$(( $fail + 1 ))
    echo -e "${red}FAILED...${norm}"
fi
echo
echo "$(( $passed + $fail )) tests performed for program ${split}"
echo "$passed test(s) passed, $fail test(s) failed"
echo

exit $fail
