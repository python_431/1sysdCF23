#!/usr/bin/env bash

split=$1

source ./test_func.sh

passed=0
fail=0

txt=$(python3 -c "print(' '.join(['spam'] * 200))")
correct="200: $(python3 -c "print(' '.join(['\"spam\"'] * 200))")"

if test_split ${split} "${txt}" "${correct}"
then
    passed=$(( $passed + 1 ))
    echo -e "${green}PASSED!${norm}"
else
    fail=$(( $fail + 1 ))
    echo -e "${red}FAILED...${norm}"
fi
echo
echo "$(( $passed + $fail )) tests performed for program ${split}"
echo "$passed test(s) passed, $fail test(s) failed"
echo

exit $fail

