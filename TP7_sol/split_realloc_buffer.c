#include<stdlib.h>
#include<string.h>

// décompose une phrase en tableau de mots
// utiliser malloc pour allouer l'espace mémoire
// de chaque élément du tableau
// ... = malloc( sizeof(char) * longueur + 1)
// ne pas oublier qu'une chaîne c'est un tableau de char 
// avec un zéro terminal
// veriante avec realloc et pointeur vers pointeur vers pointeur
// vers char (:-D)
// cette fois on initialise un buffer qui a la taille de 
// la chaîne entière pour stocker chaque mot (max possible
// pour un mot) et on le libère à la fin
// aussi on ne lit qu'une fois chaque caractère de la la
// chaîne, pas de retour en arrière pour lire chaque mot,
// ils sont copiés dans le buffer caractère par caractère
// au fur et à mesure

int split(char *msg, char ***words) {
	char *p, *buffer;
	int i = 0; // indice pour parcourir chaque mot trouvé
	int n; // nombre de mots trouvés

	// buffer pour stocker chaque mot 
	buffer = malloc((strlen(msg) + 1) * sizeof(char));

	p = msg;
	n = 0;
	// on avance jusqu'au premier caractère non-espace
	while ( *p == ' ' ) {
		p++;
	}
	while (1) {
	   buffer[i] = *p;
	   if ( *p == ' ' || *p == '\0' ) { // fin du mot dépassée
		if (i == 0) { // on a vraiment fini
			break;
		}
	        buffer[i] = '\0';
		*words = reallocarray(*words, n + 1, sizeof(char *));
		(*words)[n] = malloc(sizeof(char) * (i + 1));
		//memcpy((*words)[n++], buffer, i + 1);
		memmove((*words)[n++], buffer, i + 1);
	   	while ( *(p + 1) == ' ' ) {
			p++;
		}
		i = 0;
	   } else {
		i++;
	   }
	   if ( *p == '\0') {
		   break;
	   }
   	   p++; 
	}
	free(buffer);
	return n;
}

void free_list(char **words, int n) {
	for ( int i = 0; i < n; i++) {
		free(words[i]);
	}
	free(words);
}
	
