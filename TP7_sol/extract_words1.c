#include<stdio.h>
#include<stdlib.h>
#include<string.h>

// décompose une phrase en tableau de mots
// utiliser malloc pour alloer l'espace mémoire
// de chaque élément du tableau
// ... = malloc( sizeof(char) * longueur + 1)
// ne pas oublier qu'une chaîne c'est un tableau de char 
// avec un zéro terminal
//
// on utilise memcpy au lieu de copier caractère par
// caractère, plus efficace si memcpy utilise une instruction
// en assembleur plus rapide qu'une boucle for
// pour le savoir : aller voir le code de memcpy dans le source
// de la libc

int main() {
	char msg[] = "to be or not to be";
	char *words[50];
	char *p;
	char *start;
	int n; // nombre de mots trouvés
	int i; // indice pour parcourir chaque mot trouvé

	p = start = msg;
	n = 0;
	while (1) 	{
	   if ( *p == ' ' || *p == '\0' ) { // fin du mot dépassée
		// p - start est la longueur du mot
		// on copie tout d'un coup dans words[n]
		// man memcpy
		words[n] = malloc(sizeof(char) * (p - start + 1));
		memmove(words[n], start, p - start);
		//for (i = 0; i < p - start; i++) {
		//	words[n][i] = *(start + i);
		//}
		// ne pas oublier le 0 terminal
	        words[n++][p-start] = '\0';  // ou 0	
		start = p + 1;
	   } 
	   if ( *p == '\0') {
		   break;
	   }
   	   p++; 
	}
	// une fois que c'est fait :
	for ( int i = 0; i < n; i++) {
		printf("\"%s\"\n", words[i]);
	}
	return 0;
}
