
norm="\x1b[00m" # man console_codes
green="\x1b[00;32m"
red="\x1b[00;31m"

function test_split() {
    prog=$1
    txt=$2
    expected=$3

    echo "Test for \"${txt}\":"
    result=$( ./${split} "${txt}" | tee /dev/stderr )
    if [ "${result}" == "${expected}" ]
    then
        return 0;
    else
        return 1;
    fi
}

