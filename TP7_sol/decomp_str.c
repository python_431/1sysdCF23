#include<stdio.h>
#include<string.h>

int main() {
	char msg[] = "to be or not to be";
	char *p;
	char l;

	// première méthode
	l = strlen(msg);
	for ( int i = 0; i < l; i++ ) {
		printf("%c\n", msg[i]);
	}

	// autre (meilleure) méthode (variantes)
	p = msg;
	// au choix :
	// while ( *p != 0 )
	// while ( *p != '\0' ) Attention : PAS *p != '0'
	// while ( *p )

	while (*p) {
		printf("%c\n", *p);
		p++;
	}
        // la façon la plus naturelle en C :	
	while (*p) {
		printf("%c\n", *p++); // idem *(p++)
	}

	return 0;
}


