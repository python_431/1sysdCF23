#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

long int slow_factorial(int n) {
	long int res = 1;
	for (int i = 1; i <= n; i++) {
		res *= i;
		printf(".");
		fflush(stdout);
		sleep(1);
	}
	printf("\n");
	return res;
}

int main(int argc, char *argv[]) {
	int n;
	pid_t pid;

	if (argc != 2) {
		printf("Usage: %s N: compute in parallel 1!, 2, ..., N! (slowly)\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	n = strtol(argv[1], NULL, 10);
	//printf("%ld\n", slow_factorial(n));

	for ( int i = 1; i <= n; i++ ) {
		if (pid = fork()) {
			printf("Fils %d lancé, calcul de %d!\n", pid, i);
		} else {
			printf("%d! = %d\n", i, slow_factorial(i));
			exit(EXIT_SUCCESS);
		}	
	}
	// on essaye de se terminer après la fin du dernier
	// fils (on pourrait vraiment attendre explicitement mais
	// nécessite l'usage des signaux UNIX : SIGCHLD
	sleep(n+1);
	printf("Fin du parent\n");
	exit(EXIT_SUCCESS);
}
