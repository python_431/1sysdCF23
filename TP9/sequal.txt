sequal en images :                         

s1 et s2 pointent sur le début de chaque chaîne et
avancent ensemble (s1++ et s2++ dans le corps du while)                            
Quand s'arrêter ? Que renvoyer ?
* quand on est sûr que les chaînes sont égales (cas 1)
* quand on est sûr que les chaînes sont différentes (cas 2)

tant que les caractères sont les mêmes et aucune fin de
chaîne n'est atteinte on continue...
 condition du while : *s1 != 0 && *s2 != 0 && *s1 == *s2
          en abrégé : *s1 && *s2 && *s1 == *s2 
Situation de sortie de boucle :

     t o t o 0          ok (égal)
s1-----------^
     t o t o 0
s2-----------^                        while( ??? ) {
======                                  s1++; 
     t o t o 0          pas ok          s2++;
s1-----^                              }
     t i t i 0                        return ???;
s2-----^
======     
     t o t o 0          pas ok
s1-----------^
     t o t o t o 0
s2-----------^
======     
     t o t o t o 0      pas ok
s1-----------^
     t o t o 0
s2-----------^
