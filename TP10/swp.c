#include<stdio.h>


// ne marche pas car ce sont des valeurs
// qui sont copiés dans la pile
void swap_notok(int x, int y) {
	int tmp;
	tmp = x;
	x = y;
	y = tmp;
}

// marche car on passe l'adresse mémoire
// des variables concernées
void swap_ok(int *x, int *y) {
	int tmp;
	tmp = *x;
	*x = *y;
	*y = tmp;
}
int main() {
	int a = 1, b = 2;

	printf("%d %d\n", a, b);
	swap_notok(a, b);
	printf("%d %d\n", a, b);
        swap_ok(&a, &b);
	printf("%d %d\n", a, b);
	return 0;
}
