#include<stdio.h>
#include<stdlib.h>

int findsub(char *str, char *subs) {
	char *p;
	int i;

	for (i = 0; str[i] != '\0'; i++) {
	    p = subs;
	    int j = i;
	    while ( *p && str[j] && *p == str[j]) {
		p++;
		j++;
	    }
	    if ( *p == '\0' ) {
		//return i;
		break;
	    }
	}
	return (*p) ? -1 : i; 
}

int main(int argc, char *argv[]) {

	if (argc != 3) {
		printf("Usage: %s string substring\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	printf("\"%s\" dans \"%s\" ?\n", argv[2], argv[1]);

	printf("%d\n", findsub(argv[1], argv[2]));
	
	exit(EXIT_SUCCESS);
}

