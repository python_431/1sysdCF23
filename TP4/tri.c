#include<stdio.h>
#include<stdlib.h>

// un classique : tri d'un tableau


// Tri d'un tablau d'entier
void tri_selection(int tab[], int n) {
    int i, j, min_idx, temp;

    // Parcourir tous les éléments du tableau sauf le dernier
    for (i = 0; i < n-1; i++) {
        // Trouver l'élément le plus petit qui suit
        min_idx = i;
        for (j = i+1; j < n; j++) {
            if (tab[j] < tab[min_idx]) {
                min_idx = j;
            }
        }
	// Échanger l'élément le plus petit avec le premier élément non trié
	printf("Permutation des éléments d'index %d %d\n",i, min_idx);
        temp = tab[i];
        tab[i] = tab[min_idx];
        tab[min_idx] = temp;
    }
}

int main() {
    int N, n, i, s;
    int *numbers;
    int min, max;

    printf("Combien de nombres voulez-vous saisir ? ");
    scanf("%d", &N); 

    numbers = malloc(N * sizeof(int));

    printf("Merci de saisir %d nombres entiers\n", N);
    s = 0;
    for (i = 0; i < N; i++) {
        printf("nombre n°%d : ", i + 1);
        scanf("%d", &n);
        numbers[i] = n;
        s += n;
    }
   
    printf("Somme : %d\n", s);
    
    printf("Vous avez saisi :");
    for (i = 0; i < N; i++) {
        printf(" %d", numbers[i]);
    }
    printf("\n");

    /* Calcul min et max */
    min = max = numbers[0];
    for (i = 1; i < N; i++) {
        if (numbers[i] < min) {
            min = numbers[i];
        }
        if (numbers[i] > max) {
            max = numbers[i];
        }
    }
    printf("Minimum : %d\n", min);
    printf("Maximum : %d\n", max);

    // Test de la fonction de tri
    printf("Tableau trié :\n");
    tri_selection(numbers, max);
    for (i = 0; i < max; i++) {
	    printf("%d ", numbers[i]);
    }
    printf("\n");

    free(numbers);
}
