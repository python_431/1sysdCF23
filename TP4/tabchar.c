#include<stdio.h>

int main() {
	char msg1[] = { 's', 'p', 'a', 'm' };
	char msg2[] = { 's', 'p', 'a', 'm', 0 };
	char msg3[] = { 's', 'p', 'a', 'm', 33, '\n', 'S', 'P', 65, 'M' , 0 };

	printf("%s\n", msg1);
	printf("%s\n", msg2);
	printf("%s\n", msg3);
	// traitons ce tableau comme un tableau de nombres
	for ( int i = 0; i < 6; i++) {
		printf(" %d", msg3[i]);
	}
	printf("\n");
	return 0;
}
