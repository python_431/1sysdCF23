#include<stdio.h>

int main() {
    int n,b;

    printf("Entrez un nombre : ");
    scanf("%d", &n);
    do {
    	printf("Entrez une base (<=10) : ");
    	scanf("%d", &b);
    } while( ! ( b > 1 && b <= 10 ) );

    while (n>0) {
	printf("%d\n", n % b);
	n = n/b; // ou n /= b;
    }
    return 0;
}	
