#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main() {
    int n = 0, secret, guess;
    int notfound = 1;

    srand(time(NULL)); 
    printf("Essayez de deviner un nombre entre 1 et 100 !\n");
    secret = rand() % 100 + 1;

    while (notfound) {
        printf("Votre idée : ");
	scanf("%d", &guess);
	n++;
	if (guess == secret) {
	    notfound = 0;
	} else {
	    if (guess > secret) {
	        printf("Trop grand...\n");
	    } else {
		printf("Trop petit...\n");
	    }
	}
    }

    printf("Gagné ! En %d coups !\n", n);
    printf("Bravo !\n");
    return 0;
} 
