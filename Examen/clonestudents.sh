#!/usr/bin/env bash

for line in $( cat liste | sed -e 's/ /_/g' -e 's/_:_/=/' )
do 
	dir=$( echo $line | cut -d'=' -f1 )
	mkdir $dir
	cd $dir
	if git clone $( echo $line | cut -d'=' -f2 ) 
	then
		echo "$dir OK!"
	else
		echo "$dir: CANNOT ACCESS REPOSITORY!"
	fi
	rm -rf */.git
	cd ..
done

