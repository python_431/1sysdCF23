#include<stdio.h>
#include<stdlib.h>
#include<string.h>
// exo_struct
// partie 1
// compléter les fonctions et ajouter du code
// de test dans main()
// https://framagit.org/jpython/1SYSD/ exo_struct
// partie 2 : créer un tableau de villes (avec deux ou trois villes)
// et affichez les infos sur toute les villes dans une boucle
// for.
typedef struct City City;
struct City {
    char *name;
    int  pop;
    long double area;
};

City ask_city() {
    City city;

    printf("Nom : ");
    // avec m scanf alloue la mémoire pour la chaîne
    // man scanf
    scanf("%ms", &city.name);
    printf("Population : ");
    scanf("%d", &city.pop);
    printf("Surface : "); 
    scanf("%Lf", &city.area);
    
    return city;
}

void print_city(City city) {
   printf("%s: %d hab. %.2Lfkm2\n", city.name, city.pop, city.area);
}

long double density(City city) {
   return city.pop / city.area;
}

int main() {
    // City paris = { "Paris", 2175601, 105.4 };
    // City londres = { "London", 8908081, 1572.0 };
    City *cities = NULL;
    int nbvilles;

    printf("Nombre de villes à saisir : ");
    scanf("%d", &nbvilles);

    for (int i = 0; i < nbvilles; i++) {
	cities = reallocarray(cities, i+1, sizeof(City));
	cities[i] = ask_city();
    }

    for (int i=0; i < nbvilles; i++) {
        print_city(cities[i]);
	printf("Densité : %.2Lf hab./km2\n", density(cities[i]));
    }

    for (int i=0; i < nbvilles; i++) {
       free(cities[i].name);
    }
    free(cities);
}
