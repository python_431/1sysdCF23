#include<stdio.h>

int main() {
	int n = 0;
	// en ASCII char (signé) suffit (de 1 à 126)
	for ( unsigned char c = 32; c < 255; c++ ) {
		printf("%c", c);
		if ( n % 72 == 0 ) {
			printf("\n");
		}
		n++;
	}
	printf("\n");
}

