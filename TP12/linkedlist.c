#include<stdio.h>
#include<stdlib.h>

typedef struct node node;
struct node {
   int val;
   node *next;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    return p;
}

void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}

node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);
   
    // note : on peut omettre tous les "!= NULL"
    // un pointeur est "faux" ssi il est NULL
    if (head == NULL) { // liste vide
        head = newnode;
    } else {            // on parcourt la liste jusqu'à la fin
        walk = head;
        while (walk->next != NULL) { // on va jusqu'au dernier nœud
            walk = walk->next;
        }
        walk->next = newnode; // on ajoute le nouvel élément
    }
    return head;
}

void append_val2(node **phead, int val) {
    node *newnode, **walk;

    newnode = create_node(val);
    
    walk = phead;
    while (*walk) {
	    walk = &( (*walk)->next );
    }
    *walk = newnode;
}

// Bon et mauvais goût en matière de code
// cf. vidéo de Linus Torvalds (TED) "L'esprit derrière Linux"
// https://www.ted.com/talks/linus_torvalds_the_mind_behind_linux?language=fr&subtitle=fr
// vers 14s

node *remove_list_entry_bad_taste(node *head, node *entry) {
	node *prev = NULL;
	node *walk = head;
        // note: si entry non présente
	// boum !
	while (walk != entry) {
		prev = walk;
		walk = walk->next;
	}

	// Remove the entry by updating th
	// head of the previous entry
        // TODO: free sur l'entrée	
	if (!prev) {
		head = entry->next;
	} else {
		prev->next = entry->next;
	}
	free(entry);
	return head;
}

// La belle version, élégante :
//
void remove_list_entry_good_taste(node **phead, node *entry) {
	node **indirect = phead;

	while ( (*indirect) != entry ) {
		indirect = &((*indirect)->next);
	}
	*indirect = entry->next;
	free(entry);
}

void insert_val(node **phead, int val) {
    node *poldhead = *phead;

    *phead = create_node(val);
    (*phead)->next = poldhead;
}

void free_list(node* head) {
    node* walk = head;
    node* next;

    while (walk) {
        next = walk->next;
        free(walk);
        walk = next;
    }
}

int find_by_value(node* start, int val) {
    int res = -1, i = 0;
    node *walk = start; 
    while (walk) {
        if (walk->val == val) {
            res = i;
	    break;
        }
        walk = walk->next;
        i++;
    }
    return res;
}

node *find_by_rank(node *start, int i) {
	node *walk = start;
	int j = 0;
	node *entry = NULL;

	while (walk) {
		if (i == j++) {
			entry = walk;
			break;
		}
		walk = walk->next;
	}
	return entry;
}

int main() {
    node *head = NULL, *current;
    node *head1 = NULL, *head2 = NULL;
    node *entry;

    // on remplit une liste à la main
    
    current = head = create_node(42);
    current->next = create_node(12);
    current = current->next;
    current->next = create_node(-5);
    current = current->next;
    current->next = create_node(41);
    print_list(head);


    head1 = append_val(head1, 42);
    head1 = append_val(head1, 12);
    head1 = append_val(head1, -5);
    head1 = append_val(head1, 41);
    
    append_val2(&head2, 42);
    append_val2(&head2, 12);
    append_val2(&head2, -5);
    append_val2(&head2, 41);

    print_list(head1);
    print_list(head2);
   
    entry = head2->next->next;
    printf("Suppression de : %d\n", entry->val);
    head2 = remove_list_entry_bad_taste(head2, entry);
    print_list(head2);

    entry = head1->next->next;
    printf("Suppression de : %d\n", entry->val);
    remove_list_entry_good_taste(&head1, entry);
    print_list(head1);
    
    insert_val(&head1, 42);
    print_list(head1);

    free_list(head);
    free_list(head1);
    free_list(head2);

    return 0;

}
