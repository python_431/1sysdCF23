# Listes simplement chaînées

## Partant de linkedlist_start.c

- Ajouter une fonction qui calcule la longeur d'une liste chaînées 
- Ajouter une fonction qui insère une valeur en fin de liste
  - La fonction reçoit un pointeur vers le premier nœud
  - Elle renvoie un donnée de même type, pourquoi c'est nécessaire ?
- On peut envisager une variante de cette fonction qui reçoit l'adresse
  d'un pointeur vers le premier nœud
  - Qu'est-ce que ça permet de plus ?
  - Le code de la fonction est-il plus simple ?

## Insérer un élément au début de la liste 

- Créer une fonction `insert(node **phead, val)` qui ajoute un 
 élément au début de la liste

## Rechercher dans la liste

- Écrire une fonction qui renvoie l'élément présent à un certain
 rang (0 : premier, 1 : second, etc.)
- Écrire une fonction qui renvoie le rang (0 : premier élément,
 1 : le second, etc.) d'une valeur si elle est présente dans
 la liste (on reverra le premier élément qui correspond) et -1 sinon
- Écrire une fonction qui renvoie un pointeur vers un élément
 de la liste s'il a une certaine valeur (encore une fois le premier
 élément présent, si il y est) et NULL sinon

## Supprimer un élément de la liste

- Écrire une fonction qui supprime le premier élément de la liste
 qui a une certaine valeur, renvoie -1 si un élément a été supprimé
 et 0 si la valeur était absente
- Même chose avec une fonction qui reçoit un pointeur vers un élément
 et le supprime s'il est présent, renvoie -1 si trouvé et supprimé
 et 0 sinon

_Note_ : pensez à libérer l'espace mémoire de l'élément supprimé

## Pour finir :

- Écrire une fonction qui libère complètement l'espace alloué à une liste
  chaînée
- Bonus : écrire une fonction qui renverse une liste chaînée


