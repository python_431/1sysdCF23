#!/usr/bin/env bash


WIDTH=$( tput cols )
HEIGHT=$( tput lines )

sed -e "s/#define WIDTH  80/#define WIDTH  $WIDTH/"  graph-80x24.c > graph.c 
sed -i -e "s/#define HEIGHT 24/#define HEIGHT $HEIGHT/" graph.c 
