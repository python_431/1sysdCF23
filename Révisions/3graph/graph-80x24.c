#include<stdlib.h>
#include<stdio.h>
#include<math.h>

#define WIDTH  80
#define HEIGHT 24

char the_graph[WIDTH][HEIGHT];

float f(float x) {
	return
#include "function.txt"
		;
}

void f_graph(float x1, float x2, float y1, float y2) {
	float x,y;
	int X,Y;
	// remplissage par des zéros
	for (int X=0; X < WIDTH; X++) {
		for (int Y=0; Y < HEIGHT; Y++) {
				the_graph[X][Y] = ' ';
		}
	}	

	// axe des x : droite y = 0
	for (X=0; X < WIDTH; X++) {
		Y = (y2 - 0)/(y2 - y1)*HEIGHT;
		the_graph[X][Y] = '-' ;
	}	
	// axe des y : droite x=0
	for (Y=0; Y < HEIGHT; Y++) {
		X = (0 - x1)*WIDTH/(x2 - x1);
		the_graph[X][Y] = '|' ;
	}	
	

	// X, Y : coordonnées écran
	// x, y : coordonnées mathématiques
	// l'arrondi float vers int se fait vers 
	// la partie entière : 15.7 -> 15
	// alors que 16 serait plus "correct"
	// donc usage de roundf (man round)
	for (X=0; X < WIDTH; X++) {
		x = X*(x2 - x1)/WIDTH + x1;
		y = f(x);
		Y = roundf( (y2 - y)*HEIGHT/(y2 - y1) );
		// vérifie que le point est bien sur l'écran
		if ( Y >= 0 && Y < HEIGHT ) {
			the_graph[X][Y] = '*' ;
		}
	}	
}

void show_graph() {
	for (int Y=0; Y < HEIGHT; Y++) {
		for (int X=0; X < WIDTH; X++) {
			printf("%c", the_graph[X][Y] );
		}
		printf("\n");
	}
}

int main() {
	f_graph(-4, 4, -16, 16);
	// f_graph(-5, 5, -1, 1);

	show_graph();

	exit(EXIT_SUCCESS);
}
