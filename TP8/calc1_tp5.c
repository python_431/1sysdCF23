#include<stdio.h>

int main() {
    double x, y;
    char op;

    printf("Premier nombre : ");
    scanf("%lf", &x);
    printf(" Second nombre : ");
    scanf("%lf", &y);
    printf("Opération [+-*/] : ");
    // hack (scanf is tricky!) 
    scanf(" %c", &op);
    switch(op) {
        case '+':
            printf("%.2f\n", x + y);
            break;
        case '-':
            printf("%.2f\n", x - y);
            break;
        case '*':
            printf("%.2f\n", x * y);
            break;
        case '/':
            printf("%.2f\n", x / y);
            break;
        default:
            printf("Opération inconnue !\n");
    }
}
