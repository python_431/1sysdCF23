#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    double x, y;
    char op;

    if (argc != 4) {
	    printf("Usage : %s x y op\n", argv[0]);
	    exit(EXIT_FAILURE);
    }

    op = argv[3][0]; // premier caractère du 3ème argument
		    
    x = strtod(argv[1], NULL);
    y = strtod(argv[2], NULL);

    switch(op) {
        case '+':
            printf("%.2f\n", x + y);
            break;
        case '-':
            printf("%.2f\n", x - y);
            break;
        case '*':
            printf("%.2f\n", x * y);
            break;
        case '/':
            printf("%.2f\n", x / y);
            break;
        default:
            printf("Opération inconnue !\n");
    }
    exit(EXIT_SUCCESS);
}
