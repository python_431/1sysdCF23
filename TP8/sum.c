#include<stdio.h>
#include<stdlib.h>

// argc : arguments count (combien d'arguments)
// argv : arguments values
//
int main(int argc, char *argv[]) {
	float sum = 0;
	int i;

	for (i = 1; i < argc; i++) {
	    sum += strtof(argv[i], NULL);
	} 

	printf("%.2f\n", sum);
	return 0;
}
