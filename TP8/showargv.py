#!/usr/bin/env python3

import sys

i = 0
for arg in sys.argv:
    print(f"{i} : {arg} type : {type(arg).__name__}")
    i += 1
