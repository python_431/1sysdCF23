// ---------------------------------------------------------------------
// exemple-processus.c
// Fichier d'exemple du livre "Solutions Temps-Reel sous Linux"
// (C) 2012 - Christophe BLAESS
// http://christophe.blaess.fr
// ---------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define LG_LIGNE 256

int main(void)
{
	char ligne[LG_LIGNE];

	while(1) {
		// Afficher un symbole d'invite (prompt)
		fprintf(stderr, "--> ");
		// Lire une ligne de commande
		if (fgets(ligne, LG_LIGNE, stdin) == NULL)
			break;
		// Supprimer le retour-chariot final
		ligne[strlen(ligne)-1] = '\0';
		// Lancer un processus
		if (fork() == 0) {
			// --- PROCESSUS FILS ---
			// Executer la commande
			execlp(ligne, ligne, NULL);
			// Message d'erreur si on echoue
			perror(ligne);
			exit(EXIT_FAILURE);
		} else {
			// --- PROCESSUS PERE ---
			// Attendre la fin de son fils
			waitpid(-1, NULL, 0);
			// Et reprendre la boucle
		}
	}
	fprintf(stderr, "\n");
	return EXIT_SUCCESS;
}

