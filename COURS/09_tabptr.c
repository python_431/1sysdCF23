#include<stdio.h>

int main() {
	int tab[] = { 42, 12, 24, 36, 17 };
        int *p;

	p = tab;

	for ( int i = 0; i < 5; i++ ) {
		printf("i = %d, adresse de p = %lx ; tab[i] = %d ; *p = %d, &tab[i] = %lx\n",
				i, p, tab[i], *p, &tab[i]);
		p++;
	}

	// Donc tab[i] c'est bien *(p + i) et donc
	// pareil que i[tab]
	for ( int i = 0; i < 5; i++ ) {
		printf(" %d", i[tab]);
	}
	printf("\n");

	return 0;
}
